
// CrossCorrelationDlg.h : ���� ���������
// author: ������ �������

#pragma once

#include "PaintRefSig.h"
#include "PaintExplrSig.h"
#include "PaintCrossCorrelation.h"
#include <vector>
#include "afxwin.h"

// ���������� ���� CCrossCorrelationDlg
class CCrossCorrelationDlg : public CDialogEx
{
// ��������
public:
	CCrossCorrelationDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CROSSCORRELATION_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
	double _xminRefSig, _xmaxRefSig,   // ����������� � ������������ ������� �� � ��� �������� �������
		_yminRefSig, _ymaxRefSig;   // ����������� � ������������ �������� �� � ��� �������� �������
	double _xminExplrSig, _xmaxExplrSig,   // ����������� � ������������ ������� �� � ��� ������������ �������
		_yminExplrSig, _ymaxExplrSig;   // ����������� � ������������ �������� �� � ��� ������������ �������
	double _xminCrrltn, _xmaxCrrltn,   // ����������� � ������������ ������� �� � ��� �������� ����������
		_yminCrrltn, _ymaxCrrltn;   // ����������� � ������������ �������� �� � ��� �������� ����������
	int _N1;   // ���������� ��� � ������� �������
	int _N2;   // ���������� ��� � ����������� �������
	double _A;   // ��������� �������� �/��� ������������ �������
	double _fo;   // ������� ������� �������� �/��� ������������ �������
	int _Br;   // �������� ��������
	double _Fs;   // ������� ������������� �������� �/��� ������������ �������
	double _dopler_freq;   // ������� ������� ��� ������������ �������
	double _dopler_freq_for_ref_sig; // ������� ������� ��� �������� �������
	double _T;   // ������ ���������
	double _d;  // ��������� ������/���(��) ������������ �������
	double _d_ref_sig;  // ��������� ������/���(��) �������� �������
	int _time_delay;   // ��������� ��������
	CStatic grpbxRefSig;   // Group Box for reference signal
	CStatic grpbxExplrSig;   // Group Box for explored signal
	CStatic grpbxSigParam;   // Group Box for signal parameters
	CStatic grpbxCAF;   // Group Box for CAF
public:
	/** ������ ������ PaintRefSig.*/
	PaintRefSig PntRefSig;
	/** ������ ������ PaintExplrSig.*/
	PaintExplrSig PntExplrSig;
	/** ������ ������ PaintCrossCorrelation.*/
	PaintCrossCorrelation PntCrssCrrltn;
	/** ������ checkbox BPSK.*/
	CButton check_BPSK;
	/** ������ checkbox MSK.*/
	CButton check_MSK;

	/** ����� ��������� ������ ����� � x, ������ ������� ������.
	* @param x - ��������, ������� ���������� ��������� �� ������� ������.
	*/
	int pow_2(int x);

	/** ������������� ������������ �������(������� � �����������).
	* ������� ������������ ����� ������������������ ���(0 � 1).
	* @param delay - ��������� �������� �������� �������.
	*/
	void CreateBitSignals(int delay);
	/** ������������ ������� ������.*/
	std::vector <bool> vec_refsig;
	/** ������������ ����������� ������.*/
	std::vector <bool> vec_explrsig;

	/** ��������� (����������� �����) ��� ���.*/
	struct cmplx
	{
		double real;
		double image;
	};

	/** ����������� �� ������� ������ ������� ������.*/
	std::vector <cmplx> Vec_RefSigEx;
	/** ����������� �� ������� ������ ����������� ������.*/
	std::vector <cmplx> Vec_ExplrSigEx;

	/** �������� ��������� BPSK.*/
	void ModulationBPSK();
	/** �������� ��������� MSK.*/
	void ModulationMSK();
	
	/** �������� ��� �� �������.
	* @param noise - ��� � �� ������������� �� ������.
	* @param &Vec_Buf - ������, �� ������� ����������� ���.
	*/
	void Noise(double noise, std::vector <double> & Vec_Buf);

	/** ���������� ���.
	* @param *data - ������ ��� ������� ���������� ���.
	* @param n - ���������� �������� � �������. 
	* @param is - ���� is = -1 - ������, ���� is = 1 - ��������.
	*/
	void fourier(struct cmplx *data, int n, int is);
	
	/** ��������� ���.*/
	void Rxy();

	/** ��������� �������� ������� ����������������.*/
	void CrossAmbFunc();
	/** ������� ����������������.*/
	std::vector<std::vector<double>> vec_amb_func;

	/** ���������� ��������� ��������.*/
	afx_msg void OnBnClickedBtn();
};
