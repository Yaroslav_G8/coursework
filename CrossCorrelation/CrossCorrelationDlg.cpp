
// CrossCorrelationDlg.cpp : ���� ����������
// author: ������ �������

#include "stdafx.h"
#include "CrossCorrelation.h"
#include "CrossCorrelationDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include"time.h"
#define _USE_MATH_DEFINES
#include"math.h"
#include<algorithm>

// ���������� ���� CCrossCorrelationDlg


CCrossCorrelationDlg::CCrossCorrelationDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_CROSSCORRELATION_DIALOG, pParent)
	, _N1(20)
	, _N2(40)
	, _A(1)
	, _fo(25)
	, _Fs(48)
	, _dopler_freq_for_ref_sig(0)
	, _dopler_freq(10)
	, _Br(9600)
	, _time_delay(10)
	, _d(0)
	, _d_ref_sig(10)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCrossCorrelationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PIC_REFSIG, PntRefSig);
	DDX_Text(pDX, IDC_EDIT1_N, _N1);
	DDX_Text(pDX, IDC_EDIT1_N2, _N2);
	DDX_Text(pDX, IDC_EDIT2_A, _A);
	DDX_Text(pDX, IDC_EDIT3_fo, _fo);
	DDX_Control(pDX, IDC_PIC_EXPLRSIG, PntExplrSig);
	DDX_Control(pDX, IDC_PIC_EXPLRSIG, PntExplrSig);
	DDX_Text(pDX, IDC_EDIT1_Br, _Br);
	DDX_Control(pDX, IDC_PIC_CROSSCRRLTN, PntCrssCrrltn);
	DDX_Control(pDX, IDC_CHECK1_BPSK, check_BPSK);
	DDX_Control(pDX, IDC_CHECK2_MSK, check_MSK);
	DDX_Text(pDX, IDC_EDIT1, _time_delay);
	DDX_Text(pDX, IDC_EDIT2, _d);
	DDX_Text(pDX, IDC_EDIT3, _Fs);
	DDX_Text(pDX, IDC_EDIT4, _dopler_freq);
	DDX_Control(pDX, IDC_GB_RS, grpbxRefSig);
	DDX_Control(pDX, IDC_GB_EXPSIG, grpbxExplrSig);
	DDX_Control(pDX, IDC_GB_SPARAM, grpbxSigParam);
	DDX_Control(pDX, IDC_GB_CAF, grpbxCAF);
}

BEGIN_MESSAGE_MAP(CCrossCorrelationDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN, &CCrossCorrelationDlg::OnBnClickedBtn)
END_MESSAGE_MAP()


// ����������� ��������� CCrossCorrelationDlg

BOOL CCrossCorrelationDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	CFont font;
	font.CreateFontW(15, 0, 0, 0, FW_NORMAL, 1, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	grpbxRefSig.SetFont(&font);
	grpbxExplrSig.SetFont(&font);
	grpbxSigParam.SetFont(&font);
	grpbxCAF.SetFont(&font);

	srand(clock());

	_T = 1. / _Br;

	_xminRefSig = 0; _xmaxRefSig = _N1;
	_yminRefSig = -1.5; _ymaxRefSig = 1.5;
	PntRefSig.Exchange(_xminRefSig, _xmaxRefSig, _yminRefSig, _ymaxRefSig, _xmaxRefSig / 8, _ymaxRefSig / 3);

	_xminExplrSig = 0; _xmaxExplrSig = _N2;
	_yminExplrSig = -1.5; _ymaxExplrSig = 1.5;
	PntExplrSig.Exchange(_xminExplrSig, _xmaxExplrSig, _yminExplrSig, _ymaxExplrSig, _xmaxExplrSig / 8, _ymaxExplrSig / 3);

	_xminCrrltn = 0; _xmaxCrrltn = _N2;
	_yminCrrltn = -1.5; _ymaxCrrltn = 1.5;
	PntCrssCrrltn.Exchange(_xminCrrltn, _xmaxCrrltn, _yminCrrltn, _ymaxCrrltn, _xmaxCrrltn / 8, _ymaxCrrltn / 3);

	UpdateData(FALSE);

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CCrossCorrelationDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CCrossCorrelationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

/** ����� ��������� ����� � x ������ ������ ������� ������.
* @param x - ��������, ������� ���������� ��������� �� ������� ������.
*/
int CCrossCorrelationDlg::pow_2(int x)
{
	x--;
	for (int p = 1; p < 32; p <<= 1)
		x |= (x >> p);
	return ++x;
}

/** ������������� ������������ �������(������� � �����������).
* ������� ������������ ����� ������������������ ���(0 � 1).
* @param delay - ��������� �������� �������� �������.
*/
void CCrossCorrelationDlg::CreateBitSignals(int delay)
{
	vec_refsig.clear();
	vec_refsig.resize(_N1);
	/** ������� ������.*/
	for (int i = 0; i < _N1; i++)
	{
		vec_refsig[i] = rand() % 2;
	}

	vec_explrsig.clear();
	vec_explrsig.resize(_N2);
	/** ����������� ������.*/
	for (int i = 0; i < _N2; i++)
	{
		vec_explrsig[i] = rand() % 2;
	}
	std::copy(vec_refsig.begin(), vec_refsig.end(), vec_explrsig.begin() + delay);
}

/** �������� ��������� BPSK.*/
void CCrossCorrelationDlg::ModulationBPSK()
{
	/** ���������� ����� � ����� ����.*/
	int amnt_pnt_in_bit = (int)(_T * _Fs * 1000);

	PntRefSig.Vec_RefSig.clear();
	PntRefSig.Vec_RefSig.resize(_N1 * amnt_pnt_in_bit);

	/** �������� �������� ��������
	* � �������� � ������ ������ ��� ��������� ����.*/
	std::vector<double> Vec_Buf_for_NoiseRe, Vec_Buf_for_NoiseIm;
	Vec_Buf_for_NoiseRe.resize(_N1 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseRe.begin(), Vec_Buf_for_NoiseRe.end(), 0);

	Vec_Buf_for_NoiseIm.resize(_N1 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseIm.begin(), Vec_Buf_for_NoiseIm.end(), 0);

	/** ����.*/
	double phase = 0;
	/** ������� ������ � ���������� ��2.*/
	for (int i = 0; i < _N1; i++)
	{
		/** �������� ������� �����, ��� ��� ������� ��������� ����, ������� sin � cos
		* �������� �������� �����������. ������� ����� �������
		* �������� cos � sin � ��������� �� -2Pi �� 2Pi. */
		for (int j = 0; j < amnt_pnt_in_bit; j++)
		{
			phase = (_dopler_freq_for_ref_sig * 1000) * (amnt_pnt_in_bit * i + j) / (_Fs * 1000);
			if (phase > 2 * M_PI)
				phase -= 2 * M_PI;
			if (phase < -2 * M_PI)
				phase += 2 * M_PI;
			/** �������� �����.*/
			PntRefSig.Vec_RefSig[amnt_pnt_in_bit * i + j].re = _A * (cos(vec_refsig[i] * M_PI) *
				cos(phase) - sin(vec_refsig[i] * M_PI) * sin(phase));
			/** ������ �����.*/
			PntRefSig.Vec_RefSig[amnt_pnt_in_bit * i + j].im = _A * (cos(vec_refsig[i] * M_PI) *
				sin(phase) + sin(vec_refsig[i] * M_PI) * cos(phase));
			/** ������ � �������� �������.*/
			Vec_Buf_for_NoiseRe[amnt_pnt_in_bit * i + j] = PntRefSig.Vec_RefSig[amnt_pnt_in_bit * i + j].re;
			Vec_Buf_for_NoiseIm[amnt_pnt_in_bit * i + j] = PntRefSig.Vec_RefSig[amnt_pnt_in_bit * i + j].im;
		}
	}

	Noise(_d_ref_sig, Vec_Buf_for_NoiseRe);
	Noise(_d_ref_sig, Vec_Buf_for_NoiseIm);

	/** ��������������� � ��� ����������� ��������.*/
	double max = 0;
	int size_vec = _N1 * amnt_pnt_in_bit;
	for (int i = 0; i < size_vec; i++)
	{
		PntRefSig.Vec_RefSig[i].re = Vec_Buf_for_NoiseRe[i];
		PntRefSig.Vec_RefSig[i].im = Vec_Buf_for_NoiseIm[i];
		if (PntRefSig.Vec_RefSig[i].re < PntRefSig.Vec_RefSig[i].im)
		{
			if (max < PntRefSig.Vec_RefSig[i].im)
				max = PntRefSig.Vec_RefSig[i].im;
		}
		else
		{
			if (max < PntRefSig.Vec_RefSig[i].re)
				max = PntRefSig.Vec_RefSig[i].re;
		}
	}

	_xmaxRefSig = PntRefSig.Vec_RefSig.size() - 1;
	_yminRefSig = -1.5 * max; _ymaxRefSig = 1.5 * max;
	PntRefSig.Exchange(_xminRefSig, _xmaxRefSig, _yminRefSig, _ymaxRefSig, _xmaxRefSig / 8, _ymaxRefSig / 3);

	/** ������������ ������� �������� ������� �� ������� ������.*/
	int num_of_sam = pow_2(_N1 * amnt_pnt_in_bit);
	Vec_RefSigEx.resize(num_of_sam);
	for (int i = 0; i < num_of_sam; i++)
	{
		if (i < (_N1 * amnt_pnt_in_bit))
		{
			Vec_RefSigEx[i].real = PntRefSig.Vec_RefSig[i].re;
			Vec_RefSigEx[i].image = PntRefSig.Vec_RefSig[i].im;
		}
		else
		{
			Vec_RefSigEx[i].real = 0;
			Vec_RefSigEx[i].image = 0;
		}
	}

	/*CFile hFile;
	CString fName;
	CString str;
	fName = "Reference signal (fourth case - BPSK).txt";
	if (!hFile.Open(fName, CFile::modeCreate | CFile::modeWrite, NULL))
	{
		MessageBox(L"File don't open!", L"Warning!", MB_OK);
	}
	str.Format(TEXT("%s"), L"real");
	hFile.Write((void*)CT2A(str), str.GetLength());
	str.Format(TEXT("\t\t\t%s\r\n"), L"image");
	hFile.Write((void*)CT2A(str), str.GetLength());
	for (int j = 0; j < num_of_sam; j++)
	{
		str.Format(TEXT("%f"), Vec_RefSigEx[j].real);
		hFile.Write((void*)CT2A(str), str.GetLength());
		str.Format(TEXT("\t\t%f\r\n"), Vec_RefSigEx[j].image);
		hFile.Write((void*)CT2A(str), str.GetLength());
	}
	hFile.Close();*/

	/** ��������� ������� �������� ��� ������������ �������.*/
	Vec_Buf_for_NoiseRe.resize(_N2 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseRe.begin(), Vec_Buf_for_NoiseRe.end(), 0);
	Vec_Buf_for_NoiseIm.resize(_N2 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_for_NoiseIm.begin(), Vec_Buf_for_NoiseIm.end(), 0);

	PntExplrSig.Vec_ExplrSig.resize(_N2 * amnt_pnt_in_bit);
	phase = 0;
	// ����������� ������ � ���������� ��2
	for (int i = 0; i < _N2; i++)
	{
		/** �������� ������� �����, ��� ��� ������� ��������� ����, ������� sin � cos
		* �������� �������� �����������. ������� ����� �������
		* �������� cos � sin � ��������� �� -2Pi �� 2Pi. */
		for (int j = 0; j < amnt_pnt_in_bit; j++)
		{
			phase = (_dopler_freq * 1000) * (amnt_pnt_in_bit * i + j) / (_Fs * 1000);
			if (phase > 2 * M_PI)
				phase -= 2 * M_PI;
			if (phase < -2 * M_PI)
				phase += 2 * M_PI;
			/** �������� �����.*/
			PntExplrSig.Vec_ExplrSig[amnt_pnt_in_bit * i + j].re = _A * (cos(vec_explrsig[i] * M_PI) *
				cos(phase) - sin(vec_explrsig[i] * M_PI) * sin(phase));
			/** ������ �����.*/
			PntExplrSig.Vec_ExplrSig[amnt_pnt_in_bit * i + j].im = _A * (cos(vec_explrsig[i] * M_PI) *
				sin(phase) + sin(vec_explrsig[i] * M_PI) * cos(phase));
			/** ������ � �������� �������.*/
			Vec_Buf_for_NoiseRe[amnt_pnt_in_bit * i + j] = PntExplrSig.Vec_ExplrSig[amnt_pnt_in_bit * i + j].re;
			Vec_Buf_for_NoiseIm[amnt_pnt_in_bit * i + j] = PntExplrSig.Vec_ExplrSig[amnt_pnt_in_bit * i + j].im;
		}
	}

	Noise(_d, Vec_Buf_for_NoiseRe);
	Noise(_d, Vec_Buf_for_NoiseIm);

	/** ��������������� � ��� ����������� ��������.*/
	max = 0;
	size_vec = _N2 * amnt_pnt_in_bit;
	for (int i = 0; i < size_vec; i++)
	{
		PntExplrSig.Vec_ExplrSig[i].re = Vec_Buf_for_NoiseRe[i];
		PntExplrSig.Vec_ExplrSig[i].im = Vec_Buf_for_NoiseIm[i];
		if (PntExplrSig.Vec_ExplrSig[i].re < PntExplrSig.Vec_ExplrSig[i].im)
		{
			if (max < PntExplrSig.Vec_ExplrSig[i].im)
				max = PntExplrSig.Vec_ExplrSig[i].im;
		}
		else
		{
			if (max < PntExplrSig.Vec_ExplrSig[i].re)
				max = PntExplrSig.Vec_ExplrSig[i].re;
		}
	}

	_xmaxExplrSig = PntExplrSig.Vec_ExplrSig.size() - 1;
	_yminExplrSig = -1.5 * max; _ymaxExplrSig = 1.5 * max;
	PntExplrSig.Exchange(_xminExplrSig, _xmaxExplrSig, _yminExplrSig, _ymaxExplrSig, _xmaxExplrSig / 8, _ymaxExplrSig / 3);

	/** ������������ ������� ������������ ������� �� ������� ������.*/
	num_of_sam = pow_2(_N2 * amnt_pnt_in_bit);
	Vec_ExplrSigEx.resize(num_of_sam);
	for (int i = 0; i < num_of_sam; i++)
	{
		if (i < (_N2 * amnt_pnt_in_bit))
		{
			Vec_ExplrSigEx[i].real = PntExplrSig.Vec_ExplrSig[i].re;
			Vec_ExplrSigEx[i].image = PntExplrSig.Vec_ExplrSig[i].im;
		}
		else
		{
			Vec_ExplrSigEx[i].real = 0;
			Vec_ExplrSigEx[i].image = 0;
		}
	}

	/*CFile hFile2;
	CString fName2;
	CString str2;
	fName2 = "Explored signal (fourth case - BPSK).txt";
	if (!hFile2.Open(fName2, CFile::modeCreate | CFile::modeWrite, NULL))
	{
		MessageBox(L"File don't open!", L"Warning!", MB_OK);
	}
	str2.Format(TEXT("%s"), L"real");
	hFile2.Write((void*)CT2A(str2), str2.GetLength());
	str2.Format(TEXT("\t\t\t%s\r\n"), L"image");
	hFile2.Write((void*)CT2A(str2), str2.GetLength());
	for (int j = 0; j < num_of_sam; j++)
	{
		str2.Format(TEXT("%f"), Vec_ExplrSigEx[j].real);
		hFile2.Write((void*)CT2A(str2), str2.GetLength());
		str2.Format(TEXT("\t\t%f\r\n"), Vec_ExplrSigEx[j].image);
		hFile2.Write((void*)CT2A(str2), str2.GetLength());
	}
	hFile2.Close();*/

	/** ������� �������� ��������.*/
	Vec_Buf_for_NoiseRe.clear();
	Vec_Buf_for_NoiseIm.clear();
}

/** �������� ��������� MSK.*/
void CCrossCorrelationDlg::ModulationMSK()
{
	/** ����� ����.*/
	double  theta = 0;
	/** ����.*/
	double phi = 0;
	/** ������� ��������.*/
	double delta_f = _Br / 4;
	/** ���������� ����� � ����� ����.*/
	int amnt_pnt_in_bit = (int)(_T * _Fs * 1000);

	/** ������������� ������������ ������.*/
	std::vector<int> vec_ref_b0;
	for (size_t s = 0; s < vec_refsig.size(); s++)
	{
		if (vec_refsig[s] == true)
			vec_ref_b0.push_back(1);
		else
			vec_ref_b0.push_back(-1);
	}

	/** �������� �������� ��������
	* � �������� � ������ ������ ��� ��������� ����.*/
	std::vector<double> Vec_Buf_Re, Vec_Buf_Im;
	Vec_Buf_Re.resize(_N1 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_Re.begin(), Vec_Buf_Re.end(), 0);

	Vec_Buf_Im.resize(_N1 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_Im.begin(), Vec_Buf_Im.end(), 0);

	PntRefSig.Vec_RefSig.clear();
	PntRefSig.Vec_RefSig.resize(_N1 * amnt_pnt_in_bit);
	/** ������� ������ � ���������� ���.*/
	int sum_ref_b0;
	for (int i = 0; i < _N1; i++)
	{
		/** �������� ������� �����, ��� ��� ������� ��������� ����, ������� sin � cos
		* �������� �������� �����������. ������� ����� �������
		* �������� cos � sin � ��������� �� -2Pi �� 2Pi. */
		for (int j = 0; j < amnt_pnt_in_bit; j++)
		{
			sum_ref_b0 = 0;
			for (int k = 0; k < i; k++)
			{
				sum_ref_b0 += (int)vec_ref_b0[k];
			}
			theta = 2 * M_PI * delta_f * ((_T * sum_ref_b0 + vec_ref_b0[i] * (j / (_Fs * 1000))));
			if (theta > 2 * M_PI)
				theta -= 2 * M_PI;
			if (theta < -2 * M_PI)
				theta += 2 * M_PI;
			phi = (_dopler_freq_for_ref_sig * 1000) * (amnt_pnt_in_bit * i + j) / (_Fs * 1000);
			if (phi > 2 * M_PI)
				phi -= 2 * M_PI;
			if (phi < -2 * M_PI)
				phi += 2 * M_PI;
			/** �������� �����.*/
			PntRefSig.Vec_RefSig[amnt_pnt_in_bit * i + j].re = _A * (cos(theta) *
				cos(phi) - sin(theta) * sin(phi));
			/** ������ �����.*/
			PntRefSig.Vec_RefSig[amnt_pnt_in_bit * i + j].im = _A * (cos(theta) *
				sin(phi) + sin(theta) * cos(phi));
			/** ������ � �������� �������.*/
			Vec_Buf_Re[amnt_pnt_in_bit * i + j] = PntRefSig.Vec_RefSig[amnt_pnt_in_bit * i + j].re;
			Vec_Buf_Im[amnt_pnt_in_bit * i + j] = PntRefSig.Vec_RefSig[amnt_pnt_in_bit * i + j].im;
		}
	}
	vec_ref_b0.clear();

	Noise(_d_ref_sig, Vec_Buf_Re);
	Noise(_d_ref_sig, Vec_Buf_Im);

	/** ��������������� � ��� ����������� ��������.*/
	double max = 0;
	int size_vec = _N1 * amnt_pnt_in_bit;
	for (int i = 0; i < size_vec; i++)
	{
		PntRefSig.Vec_RefSig[i].re = Vec_Buf_Re[i];
		PntRefSig.Vec_RefSig[i].im = Vec_Buf_Im[i];
		if (PntRefSig.Vec_RefSig[i].re < PntRefSig.Vec_RefSig[i].im)
		{
			if (max < PntRefSig.Vec_RefSig[i].im)
				max = PntRefSig.Vec_RefSig[i].im;
		}
		else
		{
			if (max < PntRefSig.Vec_RefSig[i].re)
				max = PntRefSig.Vec_RefSig[i].re;
		}
	}

	_xmaxRefSig = PntRefSig.Vec_RefSig.size() - 1;
	_yminRefSig = -1.5 * max; _ymaxRefSig = 1.5 * max;
	PntRefSig.Exchange(_xminRefSig, _xmaxRefSig, _yminRefSig, _ymaxRefSig, _xmaxRefSig / 8, _ymaxRefSig / 3);

	/** ������������ ������� �������� ������� �� ������� ������.*/
	int num_of_sam = pow_2(_N1 * amnt_pnt_in_bit);
	Vec_RefSigEx.resize(num_of_sam);
	for (int i = 0; i < num_of_sam; i++)
	{
		if (i < (_N1 * amnt_pnt_in_bit))
		{
			Vec_RefSigEx[i].real = PntRefSig.Vec_RefSig[i].re;
			Vec_RefSigEx[i].image = PntRefSig.Vec_RefSig[i].im;
		}
		else
		{
			Vec_RefSigEx[i].real = 0;
			Vec_RefSigEx[i].image = 0;
		}
	}

	/*CFile hFile;
	CString fName;
	CString str;
	fName = "Reference signal (fourth case - MSK).txt";
	if (!hFile.Open(fName, CFile::modeCreate | CFile::modeWrite, NULL))
	{
		MessageBox(L"File don't open!", L"Warning!", MB_OK);
	}
	str.Format(TEXT("%s"), L"real");
	hFile.Write((void*)CT2A(str), str.GetLength());
	str.Format(TEXT("\t\t\t%s\r\n"), L"image");
	hFile.Write((void*)CT2A(str), str.GetLength());
	for (int j = 0; j < num_of_sam; j++)
	{
		str.Format(TEXT("%f"), Vec_RefSigEx[j].real);
		hFile.Write((void*)CT2A(str), str.GetLength());
		str.Format(TEXT("\t\t%f\r\n"), Vec_RefSigEx[j].image);
		hFile.Write((void*)CT2A(str), str.GetLength());
	}
	hFile.Close();*/

	PntExplrSig.Vec_ExplrSig.clear();
	PntExplrSig.Vec_ExplrSig.resize(_N2 * amnt_pnt_in_bit);
	/** ������������� ������������ ������*/
	std::vector<int> vec_explr_b0;
	for (size_t s = 0; s < vec_explrsig.size(); s++)
	{
		if (vec_explrsig[s] == true)
			vec_explr_b0.push_back(1);
		else
			vec_explr_b0.push_back(-1);
	}

	Vec_Buf_Re.resize(_N2 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_Re.begin(), Vec_Buf_Re.end(), 0);
	Vec_Buf_Im.resize(_N2 * amnt_pnt_in_bit);
	std::fill(Vec_Buf_Im.begin(), Vec_Buf_Im.end(), 0);
	theta = 0;
	phi = 0;
	int sum_explr_b0;
	// ����������� ������ � ���������� ���
	for (int i = 0; i < _N2; i++)
	{
		/** �������� ������� �����, ��� ��� ������� ��������� ����, ������� sin � cos
		* �������� �������� �����������. ������� ����� �������
		* �������� cos � sin � ��������� �� -2Pi �� 2Pi. */
		for (int j = 0; j < amnt_pnt_in_bit; j++)
		{
			sum_explr_b0 = 0;
			for (int k = 0; k < i; k++)
			{
				sum_explr_b0 += (int)vec_explr_b0[k];
			}
			theta = 2 * M_PI * delta_f * ((_T * sum_explr_b0 + vec_explr_b0[i] * (j / (_Fs * 1000))));
			if (theta > 2 * M_PI)
				theta -= 2 * M_PI;
			if (theta < -2 * M_PI)
				theta += 2 * M_PI;
			phi = (_dopler_freq * 1000) * (amnt_pnt_in_bit * i + j) / (_Fs * 1000);
			if (phi > 2 * M_PI)
				phi -= 2 * M_PI;
			if (phi < -2 * M_PI)
				phi += 2 * M_PI;
			/** �������� �����.*/
			PntExplrSig.Vec_ExplrSig[amnt_pnt_in_bit * i + j].re = _A * (cos(theta) *
				cos(phi) - sin(theta) * sin(phi));
			/** ������ �����.*/
			PntExplrSig.Vec_ExplrSig[amnt_pnt_in_bit * i + j].im = _A * (cos(theta) *
				sin(phi) + sin(theta) * cos(phi));
			/** ������ � �������� �������.*/
			Vec_Buf_Re[amnt_pnt_in_bit * i + j] = PntExplrSig.Vec_ExplrSig[amnt_pnt_in_bit * i + j].re;
			Vec_Buf_Im[amnt_pnt_in_bit * i + j] = PntExplrSig.Vec_ExplrSig[amnt_pnt_in_bit * i + j].im;
		}
	}
	vec_explr_b0.clear();

	Noise(_d, Vec_Buf_Re);
	Noise(_d, Vec_Buf_Im);

	max = 0;
	size_vec = _N2 * amnt_pnt_in_bit;
	for (int i = 0; i < size_vec; i++)
	{
		PntExplrSig.Vec_ExplrSig[i].re = Vec_Buf_Re[i];
		PntExplrSig.Vec_ExplrSig[i].im = Vec_Buf_Im[i];
		if (PntExplrSig.Vec_ExplrSig[i].re < PntExplrSig.Vec_ExplrSig[i].im)
		{
			if (max < PntExplrSig.Vec_ExplrSig[i].im)
				max = PntExplrSig.Vec_ExplrSig[i].im;
		}
		else
		{
			if (max < PntExplrSig.Vec_ExplrSig[i].re)
				max = PntExplrSig.Vec_ExplrSig[i].re;
		}
	}

	_xmaxExplrSig = PntExplrSig.Vec_ExplrSig.size() - 1;
	_yminExplrSig = -1.5 * max; _ymaxExplrSig = 1.5 * max;
	PntExplrSig.Exchange(_xminExplrSig, _xmaxExplrSig, _yminExplrSig, _ymaxExplrSig, _xmaxExplrSig / 8, _ymaxExplrSig / 3);

	/** ������������ ������� ������������ ������� �� ������� ������.*/
	num_of_sam = pow_2(_N2 * amnt_pnt_in_bit);
	Vec_ExplrSigEx.resize(num_of_sam);
	for (int i = 0; i < num_of_sam; i++)
	{
		if (i < (_N2 * amnt_pnt_in_bit))
		{
			Vec_ExplrSigEx[i].real = PntExplrSig.Vec_ExplrSig[i].re;
			Vec_ExplrSigEx[i].image = PntExplrSig.Vec_ExplrSig[i].im;
		}
		else
		{
			Vec_ExplrSigEx[i].real = 0;
			Vec_ExplrSigEx[i].image = 0;
		}
	}

	/*CFile hFile2;
	CString fName2;
	CString str2;
	fName2 = "Explored signal (fourth case - MSK).txt";
	if (!hFile2.Open(fName2, CFile::modeCreate | CFile::modeWrite, NULL))
	{
		MessageBox(L"File don't open!", L"Warning!", MB_OK);
	}
	str2.Format(TEXT("%s"), L"real");
	hFile2.Write((void*)CT2A(str2), str2.GetLength());
	str2.Format(TEXT("\t\t\t%s\r\n"), L"image");
	hFile2.Write((void*)CT2A(str2), str2.GetLength());
	for (int j = 0; j < num_of_sam; j++)
	{
		str2.Format(TEXT("%f"), Vec_ExplrSigEx[j].real);
		hFile2.Write((void*)CT2A(str2), str2.GetLength());
		str2.Format(TEXT("\t\t%f\r\n"), Vec_ExplrSigEx[j].image);
		hFile2.Write((void*)CT2A(str2), str2.GetLength());
	}
	hFile2.Close();*/

	/** ������� �������� ��������.*/
	Vec_Buf_Re.clear();
	Vec_Buf_Im.clear();
}

/** �������� ��� �� �������.
* @param noise - ��� � �� ������������� �� ������.
* @param &Vec_Buf - ������, �� ������� ����������� ���.
*/
void CCrossCorrelationDlg::Noise(double noise, std::vector <double> & Vec_Buf)
{
	size_t size = Vec_Buf.size();
	// ��������� ����
	double sum_kv_n = 0;
	int M = 0;
	double *arr_n = new double[size];
	for (size_t i = 0; i < size; i++)
	{
		M = rand() % 9 + 12;
		double sum_Qsi = 0;
		for (int j = 0; j < M; j++)
		{
			sum_Qsi += (rand() % 21 - 10) / 10.;
		}
		arr_n[i] = sum_Qsi / M;
		sum_kv_n += arr_n[i] * arr_n[i];
	};

	double sum_kv_Sig = 0;
	for (size_t t = 0; t < size; t++)
	{
		sum_kv_Sig += Vec_Buf[t] * Vec_Buf[t];
	};

	double _alpha = 0;
	_alpha = sqrt(pow(10, -noise / 10) * sum_kv_Sig / (sum_kv_n));

	// ��������� ������� ���������� � �����
	for (size_t i = 0; i < size; i++)
	{
		Vec_Buf[i] = Vec_Buf[i] + _alpha * arr_n[i];
	};

}

/** ���������� ���.
* @param *data - ������ ��� ������� ���������� ���.
* @param n - ���������� �������� � �������.
* @param is - ���� is = -1 - ������, ���� is = 1 - ��������.
*/
void CCrossCorrelationDlg::fourier(struct cmplx *data, int n, int is)
{
	int i, j, istep;
	int m, mmax;
	double r,
		r1,
		theta,
		w_r,
		w_i,
		temp_r,
		temp_i;
	double pi = 3.1415926f;

	r = pi*is;   // ������ ��� ���������
	j = 0;
	for (i = 0; i < n; i++)
	{
		if (i < j)
		{
			temp_r = data[j].real;
			temp_i = data[j].image;
			data[j].real = data[i].real;
			data[j].image = data[i].image;
			data[i].real = temp_r;
			data[i].image = temp_i;
		}
		m = n >> 1;
		while (j >= m)
		{
			j -= m; m = (m + 1) / 2;
		}
		j += m;
	}
	mmax = 1;
	while (mmax < n)
	{
		istep = mmax << 1;
		r1 = r / (double)mmax;
		for (m = 0; m < mmax; m++)
		{
			theta = r1*m;
			w_r = (double)cos((double)theta);
			w_i = (double)sin((double)theta);
			for (i = m; i < n; i += istep)
			{
				j = i + mmax;
				temp_r = w_r*data[j].real - w_i*data[j].image;
				temp_i = w_r*data[j].image + w_i*data[j].real;
				data[j].real = data[i].real - temp_r;
				data[j].image = data[i].image - temp_i;
				data[i].real += temp_r;
				data[i].image += temp_i;
			}
		}
		mmax = istep;
	}
	if (is > 0)
		for (i = 0; i < n; i++)
		{
			data[i].real /= (double)n;
			data[i].image /= (double)n;
		}

}

/** ��������� ���.*/
void CCrossCorrelationDlg::Rxy()
{
	PntCrssCrrltn.Vec_Crrltn.clear();
	/** ������ ���.*/
	size_t sizeCrrltn = PntExplrSig.Vec_ExplrSig.size() - PntRefSig.Vec_RefSig.size();

	double max = 0;
	double ValRe, ValIm;
	for (size_t i = 0; i < sizeCrrltn; i++)
	{
		ValRe = 0;
		ValIm = 0;
		for (size_t j = 0; j < sizeCrrltn; j++)
		{
			ValRe += (PntRefSig.Vec_RefSig[j].re * PntExplrSig.Vec_ExplrSig[j + i].re +
				PntRefSig.Vec_RefSig[j].im * PntExplrSig.Vec_ExplrSig[j + i].im);
			ValIm += (PntRefSig.Vec_RefSig[j].re * (-PntExplrSig.Vec_ExplrSig[j + i].im) +
				PntRefSig.Vec_RefSig[j].im * PntExplrSig.Vec_ExplrSig[j + i].re);
		}
		PntCrssCrrltn.Vec_Crrltn.push_back(1.f / sizeCrrltn * sqrt(ValRe * ValRe + ValIm * ValIm));
		if (max < PntCrssCrrltn.Vec_Crrltn[i])
			max = PntCrssCrrltn.Vec_Crrltn[i];
	}

	_xmaxCrrltn = sizeCrrltn - 1;
	_yminCrrltn = -max - max / 2;	_ymaxCrrltn = max + max / 2;
	PntCrssCrrltn.Exchange(_xminCrrltn, _xmaxCrrltn, _yminCrrltn, _ymaxCrrltn, _xmaxCrrltn / 8, _ymaxCrrltn / 3);
}

/** ��������� �������� ������� ����������������.*/
void CCrossCorrelationDlg::CrossAmbFunc()
{
	PntCrssCrrltn.Vec_Crrltn.clear();

	/** ������ ������� ������������ �������.*/
	size_t size_vec_explr_sig = Vec_ExplrSigEx.size();

	/** ������ ������� �������� �������.*/
	size_t size_vec_ref_sig = Vec_RefSigEx.size();

	/** �������� size_vec_explr_sig � size_vec_ref_sig.*/
	size_t size_diff = size_vec_explr_sig - size_vec_ref_sig;

	/** ��������� ������ ��� �������.*/
	vec_amb_func.resize(size_diff);
	for (size_t i = 0; i < size_diff; i++)
	{
		vec_amb_func[i].resize(size_vec_ref_sig);
	}

	/** �������� ������ ��� ���.*/
	std::vector<cmplx> vec_buf;
	vec_buf.resize(size_vec_ref_sig);

	double val_re, val_im;
	double max1 = 0;
	/** ���������� ��.*/
	for (size_t i = 0; i < size_diff; i++)
	{
		val_re = 0;
		val_im = 0;
		std::fill(vec_buf.begin(), vec_buf.end(), cmplx{ 0, 0 });
		for (size_t j = 0; j < size_vec_ref_sig; j++)
		{
			vec_buf[j].real = (Vec_RefSigEx[j].real * Vec_ExplrSigEx[j + i].real +
				Vec_RefSigEx[j].image * Vec_ExplrSigEx[j + i].image);
			val_re += vec_buf[j].real;
			vec_buf[j].image = (Vec_RefSigEx[j].real * (-Vec_ExplrSigEx[j + i].image) +
				Vec_RefSigEx[j].image * Vec_ExplrSigEx[j + i].real);
			val_im += vec_buf[j].image;
		}

		val_re /= size_vec_ref_sig;
		val_im /= size_vec_ref_sig;

		for (size_t j = 0; j < size_vec_ref_sig; j++)
		{
			vec_buf[j].real -= val_re;
			vec_buf[j].image -= val_im;
		}

		max1 = 0;
		fourier(vec_buf.data(), size_vec_ref_sig, -1);
		for (size_t k = 0; k < size_vec_ref_sig; k++)
		{
			vec_amb_func[i][k] = sqrt(vec_buf[k].real * vec_buf[k].real + vec_buf[k].image * vec_buf[k].image);
			if (max1 < vec_amb_func[i][k])
				max1 = vec_amb_func[i][k];
		}
		PntCrssCrrltn.Vec_Crrltn.push_back(max1);
	}

	double max2 = 0;
	for (size_t it = 0; it < PntCrssCrrltn.Vec_Crrltn.size(); it++)
	{
		if (max2 < PntCrssCrrltn.Vec_Crrltn[it])
			max2 = PntCrssCrrltn.Vec_Crrltn[it];
	}
	/** ���������� �������� �� � ������������� ��������� [0;1].*/
	std::for_each(PntCrssCrrltn.Vec_Crrltn.begin(), PntCrssCrrltn.Vec_Crrltn.end(),
		[max2](double & x) { x /= max2; });

	max2 = 0;
	for (size_t it = 0; it < PntCrssCrrltn.Vec_Crrltn.size(); it++)
	{
		if (max2 < PntCrssCrrltn.Vec_Crrltn[it])
			max2 = PntCrssCrrltn.Vec_Crrltn[it];
	}

	_xmaxCrrltn = PntCrssCrrltn.Vec_Crrltn.size() - 1;
	_yminCrrltn = -max2 - max2 / 2;	_ymaxCrrltn = max2 + max2 / 2;
	PntCrssCrrltn.Exchange(_xminCrrltn, _xmaxCrrltn, _yminCrrltn, _ymaxCrrltn, _xmaxCrrltn / 8, _ymaxCrrltn / 3);

	/*CFile hFile;
	CString fName;
	CString str;
	fName = "Cross-ambiguity function.txt";
	if (!hFile.Open(fName, CFile::modeCreate | CFile::modeWrite, NULL))
	{
		MessageBox(L"File don't open!", L"Warning!", MB_OK);
	}

	for (size_t i = 0; i < size_diff; i++)
	{
		str.Format(TEXT("%s\r\n"), L"Cross-ambuguity function");
		hFile.Write((void*)CT2A(str), str.GetLength());
		for (int j = 0; j < size_vec_ref_sig; j++)
		{
			str.Format(TEXT("%f\r\n"), vec_amb_func[i][j]);
			hFile.Write((void*)CT2A(str), str.GetLength());
		}
	}

	hFile.Close();*/
}

/** ���������� ��������� ��������.*/
void CCrossCorrelationDlg::OnBnClickedBtn()
{
	UpdateData(TRUE);

	CreateBitSignals(_time_delay);

	if (check_BPSK.GetCheck())
	{
		ModulationBPSK();
		//Rxy();
		CrossAmbFunc();
	}
	else if (check_MSK.GetCheck())
	{
		ModulationMSK();
		//Rxy();
		CrossAmbFunc();
	}

	UpdateData(FALSE);
}
